local live = require "alive" (1280, 720, {
  globals = _G
})

live.enabled = true

rl.SetConfigFlags(rl.FLAG_VSYNC_HINT + rl.FLAG_WINDOW_RESIZABLE)

rl.InitWindow(1280, 720, "raylib [lua] example - LIVE variable explorer")

while not rl.WindowShouldClose() do
  live:update(rl.GetFrameTime())

	rl.BeginDrawing()

	rl.ClearBackground(rl.BLACK)
  live:draw()

	rl.EndDrawing()
end

rl.CloseWindow()
