local screenWidth = 1280
local screenHeight = 720

rl.SetConfigFlags(rl.FLAG_VSYNC_HINT)
rl.InitWindow(screenWidth, screenHeight, "raylua [models] example - geometric shapes")

local camera = rl.new("Camera", {
  position = rl.new("Vector3", 0, 10, 10),
  target = rl.new("Vector3", 0, 0, 0),
  up = rl.new("Vector3", 0, 1, 0),
  fovy = 45,
  type = rl.CAMERA_PERSPECTIVE
})

local shapes = {
  { "Cube", { { -4, 0, 2 }, 2, 5, 2, rl.RED } },
  { "CubeWires", { { -4, 0, 2 }, 2, 5, 2, rl.GOLD } },
  { "CubeWires", { { -4, 0, -2 }, 3, 6, 2, rl.MAROON } },

  { "Sphere", { { 1, 0, -2 }, 1, rl.GREEN } },
  { "SphereWires", { { 1, 0, 2, }, 2, 16, 16, rl.LIME } },

  { "Cylinder", { { 4, 0, -2 }, 1, 2, 3, 4, rl.SKYBLUE } },
  { "CylinderWires", { { 4, 0, -2 }, 1, 2, 3, 4, rl.DARKBLUE } },
  { "CylinderWires", { { 4.5, -1, 2 }, 1, 1, 2, 6, rl.BROWN } },

  { "Cylinder", { { 1, 0, -4 }, 0, 1.5, 3, 8, rl.GOLD } },
  { "CylinderWires", { { 1, 0, -4 }, 0, 1.5, 3, 8, rl.PINK } }
}

local live = require "alive" (screenWidth, screenHeight, {
  globals = _G,
  shapes = shapes,
})

while not rl.WindowShouldClose() do
  rl.BeginDrawing()
  rl.ClearBackground(rl.RAYWHITE)

  live:update(rl.GetFrameTime())

  if rl.IsKeyPressed(rl.KEY_F1) then
    live.enabled = not live.enabled
  end

  rl.BeginMode3D(camera)

  for _,shape in pairs(shapes) do
    local func = rl["Draw" .. shape[1]](unpack(shape[2]))
  end

  rl.DrawGrid(10, 1)

  rl.EndMode3D()

  rl.DrawFPS(10, 10)
  live:draw()

  rl.EndDrawing()
end

rl.CloseWindow()
