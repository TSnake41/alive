local base = (...)
local lynx = require(base .. ".lynx")
local handlers

local input_keys = {
  { rl.KEY_ENTER, "enter" },
  { rl.KEY_LEFT, "left" },
  { rl.KEY_RIGHT, "right" }
}

local live = {}

local function make_tree(var, name)
  if type(var) ~= "table" then
    var = { value = var }
  end

  local name = string.format("%s [%s]", name, type(var))

  local list = {
    lynx.button("(hide)", function (self, menu)
      menu:push {
        items = {
          lynx.button("(show)", function (self, menu)
            menu:pop()
          end, { align = "left" })
        }
      }
    end),
    lynx.text(name, {
      selectable = false,
      text_color = { .5, .7, .7, 1 }
    }),
    lynx.button("<<<", function (self, menu) menu:pop() end),
    { selectable = false }
  }

  for k,v in pairs(var) do
    for i,h in ipairs(handlers) do
      if h.filter(v) then
        list[#list + 1] = h.handler(var, k)
        break
      end
    end
  end

  for i,v in ipairs(list) do
    v.align = "left"
  end

  return list
end

function live:new(width, height, vars, font)
  local self = setmetatable({}, live.mt)

  self.w = width
  self.h = height

  self.font = font or rl.GetFontDefault() -- unsupported
  self.enabled = false

  self.font_height = font and font.baseSize or 20
  self.speed = 1

  vars.live = self

  self.menu = lynx.menu(make_tree(vars, "root"), {
    x = 0, y = 0,
    ox = 5, oy = self.font_height * 2 + 10,
    w = width, h = height - 30,
    default_height = self.font_height,
    funcs = lynx.raylua_lynx
  })

  self.show_live = true
  self.show_stats = true
  self.use_canvas = false

  return self
end

function live:update(dt)
  if self.enabled then
    if not self.moving then
      local pos = rl.GetMousePosition()

      if rl.IsMouseButtonPressed(rl.MOUSE_LEFT_BUTTON) then
        self.menu:input_mouse(pos.x, pos.y, 1)
      elseif rl.IsMouseButtonPressed(rl.MOUSE_RIGHT_BUTTON) then
        self.menu:input_mouse(pos.x, pos.y, 2)
        self.menu:pop()
      end

      self.menu:input_mouse(pos.x, pos.y, 0)
    end

    if rl.IsKeyDown(rl.KEY_LEFT_CONTROL) then
      self.moving = true
      if rl.IsKeyPressed(rl.KEY_R) then
        self.menu.ox = 5
        self.menu.oy = self.font_height * 2 + 10
      end

      self.speed = rl.IsKeyDown(rl.KEY_LEFT_SHIFT) and 5 or 1

      local x, y = 0, 0
      if rl.IsKeyDown(rl.KEY_LEFT) then
        x = x - 1
      end
      if rl.IsKeyDown(rl.KEY_RIGHT) then
        x = x + 1
      end
      if rl.IsKeyDown(rl.KEY_UP) then
        y = y - 1
      end
      if rl.IsKeyDown(rl.KEY_DOWN) then
        y = y + 1
      end

      self.menu.ox = self.menu.ox + x * dt * 200 * self.speed
      self.menu.oy = self.menu.oy + y * dt * 200 * self.speed
    else
      self.moving = false

      if rl.IsKeyPressed(rl.KEY_BACKSPACE) then
        self.menu:pop()
      end

      for k,v in pairs(input_keys) do
        if rl.IsKeyPressed(v[1]) then
          self.menu:input_key(v[2], "pressed")
        end

        if rl.IsKeyDown(v[1]) then
          self.menu:input_key(v[2], "down")
        end
      end
    end
  end

  --text:update()
  self.menu:update(dt)
end

function live:draw(x, y)
  if self.enabled then

  rl.DrawRectangle(0, 0, self.w, self.h, rl.new("Color", 0, 0, 0, 64))

  if self.moving then
    rl.DrawRectangle(0, 0, self.w, self.h, rl.new("Color", 192, 191, 192, 128))
    rl.DrawText(string.format("X:%.1f Y:%.1f", self.menu.ox, self.menu.oy), 5, 45, 20, rl.WHITE)
  end

  self.menu:draw()

  if self.show_live then
    rl.DrawText("Another LIVE Integrated Variable Explorer", 5, 5, 20, rl.new("Color", 64, 128, 255, 255))
  end

  if self.show_stats then
    rl.DrawText (
      string.format("FPS: %g | Frametime: %.1f ms | Memory: %g kb", rl.GetFPS(),
      rl.GetFrameTime() * 1000, math.ceil(collectgarbage 'count')), 5,
      self.font_height + 5, 20, rl.new("Color", 192, 128, 255, 255)
    )
  end
  end
end

handlers = require(base .. ".handlers")(lynx, make_tree)
live.make_tree = make_tree
live.handlers = handlers

live.mt = {
  __index = live,
}

live.__call = live.new
return setmetatable(live, live)
