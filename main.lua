local screenWidth, screenHeight = 800, 450
local lynx = require "alive.lynx"

local function launch(demo)
  rl.CloseWindow()

  require(demo)

  rl.SetConfigFlags(rl.FLAG_VSYNC_HINT)
  rl.InitWindow(screenWidth, screenHeight, "Alive demo menu")
end

local menu = lynx.menu ({
  lynx.text("Alive demo menu", { selectable = false }),
  lynx.text("", { selectable = false }),
  lynx.button("basic", function () launch "demos.basic" end),
  lynx.button("shapes list", function () launch "demos.shapes_list" end)
}, {
  x = 0, y = 0,
  w = screenWidth,
  h = screenHeight,
  default_height = 24,
  current = 3,
  funcs = lynx.raylua_lynx
})

rl.SetConfigFlags(rl.FLAG_VSYNC_HINT)
--rl.SetTargetFPS(60)

rl.InitWindow(screenWidth, screenHeight, "Alive demo menu")

local time = 0
local background = rl.new("Color", 0, 0, 0, 255)

while not rl.WindowShouldClose() do
  local dt = rl.GetFrameTime()

  do -- Mouse handling
    local pos = rl.GetMousePosition()

    if rl.IsMouseButtonPressed(rl.MOUSE_LEFT_BUTTON) then
      menu:input_mouse(pos.x, pos.y, 1)
    elseif rl.IsMouseButtonPressed(rl.MOUSE_RIGHT_BUTTON) then
      menu:input_mouse(pos.x, pos.y, 2)
      menu:pop()
    end

    menu:input_mouse(pos.x, pos.y, 0)
  end

  menu:update(dt)

	rl.BeginDrawing()
	rl.ClearBackground(background)
  menu:draw()
	rl.EndDrawing()
end

rl.CloseWindow()
